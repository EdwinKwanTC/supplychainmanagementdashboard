import { useState, useEffect } from 'react';
import ReactApexChart from 'react-apexcharts';

// this function get the innerWidth
const getWindowDimensions = () => {
    const { innerWidth: width } = window;

    return {
        width,
    };
};

const BarAndLineChart = () => {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    useEffect(() => {
        function handleResize() {
            setWindowDimensions(getWindowDimensions());
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    const options = {
        xaxis: {
            categories: [
                'product1',
                'product2',
                'product3',
                'product4',
                'product5',
                'product6',
                'product7',
                'product8',
                'product9',
                'product10',
            ],
        },
    };

    const series = [
        {
            name: 'Inventory',
            data: [31, 40, 28, 51, 42, 109, 100, 31, 40, 28],
            type: 'line',
        },
        {
            name: 'Days of Inventory',
            data: [11, 32, 45, 32, 34, 52, 41, 31, 40, 28],
            type: 'column',
        },
    ];

    return (
        <ReactApexChart
            type="line"
            options={options}
            series={series}
            width={windowDimensions.width * 0.9}
            height={300}
        />
    );
};

export default BarAndLineChart;
