import { useState, useEffect } from 'react';
import './HorizontalBarChart.scss';

// functions
import ReactApexChart from 'react-apexcharts';

// this function get the innerWidth
const getWindowDimensions = () => {
    const { innerWidth: width } = window;

    return {
        width,
    };
};

type ITextStyle = {
    textAnchor: 'start' | 'middle' | 'end' | undefined;
    style: { colors: string };
};

const HorizontalBarChart = () => {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    const textAnchor: ITextStyle = { textAnchor: 'start', style: { colors: '#555' } };

    useEffect(() => {
        function handleResize() {
            setWindowDimensions(getWindowDimensions());
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    const options = {
        plotOptions: {
            bar: {
                horizontal: true,
            },
        },
        xaxis: {
            categories: [
                'Arrive Customer address to Goods Receipt',
                'Pick to Arrive Customer address',
                'Customer Order to pick',
                'Arrive Inbound Airport to warehouse',
                'Arrive Outbound Airpot to Arrive Inbound',
                'Exit Factory to Arrive Outbound Airpoty',
                'Confirmation to Exit Factory',
            ],
        },
        dataLabels: {
            textAnchor: textAnchor.textAnchor,
            style: { colors: [textAnchor.style] },
        },
    };

    const series = [
        {
            data: [0.03, 0.8, 0.5, 0.3, 0.4, 0.7, 14],
        },
    ];

    return (
        <div className="horizontalBarChart">
            <ReactApexChart
                type="bar"
                options={options}
                series={series}
                width={windowDimensions.width * 0.9}
                height={300}
            />
            <div className="yaxis">Day(s)</div>
        </div>
    );
};

export default HorizontalBarChart;
