import './Supplier.scss';

// css library
import Button from '@mui/material/Button';

// react roter
import { Link } from 'react-router-dom';

// components
import SupplierList from './SupplierList';

// useage
// This is the page /supplier, it shows the list of all supplier.
// And it provide users way to enter edit supplier page or add new Delivery

const Supplier = () => {
    return (
        <div className="supplier">
            <div className="buttonGroup">
                <Button variant="contained" className="button">
                    <Link style={{ textDecoration: 'none', color: '#FFF' }} to="/new-supplier">
                        New Supplier
                    </Link>
                </Button>
                <Button variant="contained" className="button">
                    <Link style={{ textDecoration: 'none', color: '#FFF' }} to="/new-delivery">
                        New Deliver
                    </Link>
                </Button>
            </div>

            <div className="supplierList">
                <SupplierList />
            </div>
        </div>
    );
};

export default Supplier;
