import { useState, useEffect } from 'react';
import './DeliveryStatusList.scss';

// css library
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { DataGrid, GridColDef, GridValueGetterParams, GridRowId } from '@mui/x-data-grid';

// interface
import { IWorkstationList } from '../../interface/workStation';
import { workStation } from '../../interface/workStation';

// sweet alert 2
import Swal from 'sweetalert2';

// moments
import moment from 'moment';

// call API functions
import { callAPI } from '../../CallAPI';

// interface
interface IProcessNextWorkStation {
    selectedShipmentIds: GridRowId[];
    currentWorkstation: string;
}

const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'productName', headerName: 'Product Name', width: 130 },
    { field: 'invoiceNo', headerName: 'invoice no.', width: 130 },
    {
        field: 'updatedAt',
        headerName: 'Arrived at',
        type: 'date',
        width: 200,
        valueGetter: (params: GridValueGetterParams) =>
            moment(params.row.updatedAt).local().format('DD/MMM/YY'),
    },
    {
        field: 'arrivalTime',
        headerName: 'task spends',
        type: 'date',
        width: 200,
        valueGetter: (params: GridValueGetterParams) =>
            moment(params.row.updatedAt).local().fromNow(),
    },
];

export default function DeliveryStatusList() {
    const [workLocation, setWorklocation] = useState('warehouse');
    const [workList, setWorkList] = useState<IWorkstationList[]>([]);
    const [selectedItem, setSelectItem] = useState<GridRowId[]>([]);

    const handleChange = (event: SelectChangeEvent) => {
        setWorklocation(event.target.value as string);
    };

    const processToNextStatus = () => {
        if (selectedItem.length === 0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Must at least select one order',
            });
        }

        callAPI<IProcessNextWorkStation, string>('POST', '/workstation-process', {
            selectedShipmentIds: selectedItem,
            currentWorkstation: workLocation,
        })
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'success',
                    text: 'supplier added successfully',
                });
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops',
                    text: err.message,
                });
            })
            .then(() =>
                callAPI<string, IWorkstationList[]>(
                    'GET',
                    `/workstation-list/${workLocation}`,
                ).then((data) => setWorkList(data)),
            );
    };

    useEffect(() => {
        callAPI<string, IWorkstationList[]>('GET', `/workstation-list/${workLocation}`).then(
            (data) => setWorkList(data),
        );
    }, [workLocation]);

    return (
        <div className="deliveryStatusList">
            <div className="selectBar">
                <Box sx={{ width: 200 }}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Work Station</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={workLocation}
                            label="work-station"
                            onChange={handleChange}
                        >
                            <MenuItem value={workStation.warehouse}>
                                Waiting for Exit Factory
                            </MenuItem>
                            <MenuItem value={workStation.exitedFactory}>Exited Factory</MenuItem>
                            <MenuItem value={workStation.arrivedOutboundAirport}>
                                Arrived Outbound Airpot
                            </MenuItem>
                            <MenuItem value={workStation.arrivedInboundAirport}>
                                Arrived Inbound Airport
                            </MenuItem>
                            <MenuItem value={workStation.arrivedLocalWarehouse}>
                                Arrived Local Warehouse
                            </MenuItem>
                            <MenuItem value={workStation.calledCustomerPick}>
                                Called Customer to Pick
                            </MenuItem>
                            <MenuItem value={workStation.arrivedCustomerAddress}>
                                Arrived Customer Address
                            </MenuItem>
                            <MenuItem value={workStation.customerPickedUp}>
                                Customer Picked Up
                            </MenuItem>
                        </Select>
                    </FormControl>
                </Box>
                <div>
                    <Button variant="contained" onClick={processToNextStatus}>
                        process
                    </Button>
                </div>
            </div>

            <div style={{ height: 600, width: '100%' }}>
                <DataGrid
                    rows={workList}
                    columns={columns}
                    pageSize={10}
                    rowsPerPageOptions={[10]}
                    checkboxSelection
                    onSelectionModelChange={(ids) => setSelectItem(ids)}
                />
            </div>
        </div>
    );
}
