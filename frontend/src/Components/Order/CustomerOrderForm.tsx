import { useEffect, useState } from 'react';
import '../Product/NewProductForm.scss';

// css library
import Button from '@mui/material/Button';
import Form from 'react-bootstrap/Form';

// react hook form
import { useForm, SubmitHandler } from 'react-hook-form';

// sweet alert 2
import Swal from 'sweetalert2';

// interface
import { IProduct } from '../../interface/product';
import { ISupplierRecords } from '../../interface/supplier';
import { ICustomerOrderInput } from '../../interface/order';

// Fetch function
import { callAPI } from '../../CallAPI';

// react router dom
import { BrowserRouter as Router, useNavigate } from 'react-router-dom';

// useage
// This Form is used for Adding New Supplier

const CustomerOrderForm = () => {
    let navigate = useNavigate();
    const { register, handleSubmit } = useForm<ICustomerOrderInput>();
    const [suppliers, setSuppliers] = useState<ISupplierRecords[]>([]);
    const [products, setProducts] = useState<IProduct[]>([]);

    useEffect(() => {
        callAPI<null, ISupplierRecords[]>('GET', '/supplier')
            .then((data) => setSuppliers(data))
            .catch((err) =>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err.message,
                }),
            );

        callAPI<null, IProduct[]>('GET', '/product')
            .then((data) => setProducts(data))
            .catch((err) =>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err.message,
                }),
            );
    }, []);

    const onSubmit: SubmitHandler<ICustomerOrderInput> = (data) => {
        const productInfo = data.productId.toString();
        const productId = +productInfo.split('-')[0];

        if (isNaN(productId)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please select a product',
            });
            return;
        }

        if (!data.customerName) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in customer name',
            });
            return;
        }

        if (data.customerName.length > 50) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of customer name',
            });
            return;
        }

        if (data.customerAddress.length > 300) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of customer address',
            });
            return;
        }

        if (!data.customerAddress) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in customer address',
            });
            return;
        }

        if (!data.quantity) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in order quantity',
            });
            return;
        }

        if (data.quantity > 1000) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid quantity of product',
            });
            return;
        }

        callAPI<ICustomerOrderInput, string>('POST', '/order', {
            customerName: data.customerName,
            customerAddress: data.customerAddress,
            productId: productId,
            quantity: +data.quantity,
        })
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'success',
                    text: 'supplier added successfully',
                });
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops',
                    text: err.message,
                });
            })
            .then(() => {
                navigate('/stock-management');
            });
    };

    return (
        <div className="newProductForm">
            <div className="title">Customer Order</div>
            <div className="formContent">
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Customer Name</Form.Label>
                        <Form.Control
                            type="string"
                            min={0}
                            placeholder="please fill in opening balance"
                            {...register('customerName')}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Customer Adress</Form.Label>
                        <Form.Control
                            type="string"
                            min={0}
                            placeholder="please fill in opening balance"
                            {...register('customerAddress')}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Select Supplier</Form.Label>
                        <Form.Select {...register('productId')}>
                            <option key={0}>Please Select Product</option>
                            {products.map((product) => (
                                <option key={product.id}>
                                    {product.id + '-' + product.productName}
                                </option>
                            ))}
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Deliver Quantity</Form.Label>
                        <Form.Control
                            type="number"
                            min={0}
                            placeholder="please fill in opening balance"
                            {...register('quantity')}
                        />
                    </Form.Group>
                    <Button variant="contained" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        </div>
    );
};

export default CustomerOrderForm;
