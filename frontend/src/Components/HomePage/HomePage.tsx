import './HomePage.scss';

// components
import DoughnutChart from '../Charts/DoughnutChart/DoughnutChart';
import BarAndLineChart from '../Charts/BarAndLineChart/BarAndLineChart';
import HorizontalBarChart from '../Charts/HorizontalBarChart/HorizontalBarChart';

const HomePage = () => {
    return (
        <div className="homePage">
            <div className="title">DashBoard</div>
            <div className="doughnutChartGroup">
                <DoughnutChart title={'Purchase Order'} data1={98} data2={3} />
                <DoughnutChart title={'Outbond Shipment'} data1={40} data2={33} />
                <DoughnutChart title={'Inbond Shipment'} data1={33} data2={40} />
                <DoughnutChart title={'Sales Order'} data1={4} data2={16} />
            </div>
            <div className="chartContainer">
                <BarAndLineChart />
            </div>
            <div className="chartContainer">
                <HorizontalBarChart />
            </div>
        </div>
    );
};

export default HomePage;
