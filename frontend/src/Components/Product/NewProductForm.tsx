import { useEffect, useState } from 'react';
import './NewProductForm.scss';

// css library
import Button from '@mui/material/Button';
import Form from 'react-bootstrap/Form';

// react hook form
import { useForm, SubmitHandler } from 'react-hook-form';

// sweet alert 2
import Swal from 'sweetalert2';

// interface
import { INewProduct } from '../../interface/product';
import { ISupplierRecords } from '../../interface/supplier';

// Fetch function
import { callAPI } from '../../CallAPI';

// react router dom
import { BrowserRouter as Router, useNavigate } from 'react-router-dom';

// useage
// This Form is used for Adding New Supplier

const NewProductForm = () => {
    let navigate = useNavigate();
    const { register, handleSubmit } = useForm<INewProduct>();
    const [suppliers, setSuppliers] = useState<ISupplierRecords[]>([]);

    useEffect(() => {
        callAPI<null, ISupplierRecords[]>('GET', '/supplier')
            .then((data) => setSuppliers(data))
            .catch((err) =>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err.message,
                }),
            );
    }, []);

    const onSubmit: SubmitHandler<INewProduct> = (data) => {
        const supplierInfo = data.supplierId.toString();
        const supplierId = +supplierInfo.split('-')[0];

        if (isNaN(supplierId)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please select a supplier',
            });
            return;
        }

        if (!data.productName) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in product name',
            });
            return;
        }

        if (data.productName.length > 50) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of product',
            });
            return;
        }

        if (!data.quantity) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in supplier contact',
            });
            return;
        }

        if (data.quantity > 1000) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid quantity of product',
            });
            return;
        }

        callAPI<INewProduct, string>('POST', '/product', {
            supplierId: supplierId,
            productName: data.productName,
            quantity: +data.quantity,
        })
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'success',
                    text: 'supplier added successfully',
                });
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops',
                    text: err.message,
                });
            })
            .then(() => {
                navigate('/stock-management');
            });
    };

    return (
        <div className="newProductForm">
            <div className="title">New Product</div>
            <div className="formContent">
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group className="mb-3">
                        <Form.Label>Select Supplier</Form.Label>
                        <Form.Select {...register('supplierId')}>
                            <option key={0}>Please Select Supplier</option>
                            {suppliers.map((supplier) => (
                                <option key={supplier.id}>
                                    {supplier.id + '-' + supplier.name}
                                </option>
                            ))}
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="please fill in product name"
                            {...register('productName')}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Opening Balance</Form.Label>
                        <Form.Control
                            type="number"
                            min={0}
                            placeholder="please fill in opening balance"
                            {...register('quantity')}
                        />
                    </Form.Group>
                    <Button variant="contained" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        </div>
    );
};

export default NewProductForm;
