import { AuthState } from './state';
import { AuthAction } from './action';
import { JWTPayload } from '../../interface/gobalState';
import jwtDecode from 'jwt-decode';

const initialState: AuthState = {
    isAuthenticated: !!localStorage.getItem('token'),
};

export const authReducer = (state: AuthState = initialState, action: AuthAction): AuthState => {
    switch (action.type) {
        case '@@auth/login_success': {
            try {
                let payload = jwtDecode<JWTPayload>(action.token);
                return {
                    isAuthenticated: true,
                    payload,
                };
            } catch (error) {
                return {
                    isAuthenticated: false,
                    errorMessage: 'Failed to decode JWT: ' + error,
                };
            }
        }
        case '@@auth/login_fail': {
            return {
                isAuthenticated: false,
                errorMessage: action.errorMessage,
            };
        }
        case '@@auth/logout': {
            localStorage.removeItem('token');
            return {
                isAuthenticated: false,
            };
        }
        default:
            return state;
    }
};
