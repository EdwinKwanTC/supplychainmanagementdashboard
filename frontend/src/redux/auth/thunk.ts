// interface
import { NativeLoginInput, NativeLoginOutput } from '../../interface/models';
import Swal from 'sweetalert2';

// function
import { callAPI } from '../../CallAPI';
import { push } from 'connected-react-router';

// redux
import { RootThunkDispatch } from '../thunk';
import { loginFailAction, loginSuccessAction, logoutAction } from './action';

export function autoLoginThunk() {
    return (dispatch: RootThunkDispatch) => {
        let token = localStorage.getItem('token');
        if (token) {
            dispatch(loginSuccessAction(token));
        } else {
            dispatch(logoutAction());
        }
    };
}

export function nativeLoginThunk(username: string, password: string) {
    return async (dispatch: RootThunkDispatch) => {
        try {
            await callAPI<NativeLoginInput, NativeLoginOutput>('POST', '/login', {
                username,
                password,
            })
                .then((data) => {
                    localStorage.setItem('token', data.jwt_token);
                    dispatch(loginSuccessAction(data.jwt_token));
                    dispatch(push('/'));
                })
                .catch((error) => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `${error}`,
                    });
                });
        } catch (error) {
            dispatch(loginFailAction('Native Login Failed: ' + error));
        }
    };
}

export function logoutThunk() {
    return (dispatch: RootThunkDispatch) => {
        localStorage.removeItem('token');
        dispatch(logoutAction());
        dispatch(push('/login'));
    };
}
