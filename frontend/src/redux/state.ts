import { RouterState } from 'connected-react-router';
import { AuthState } from './auth/state';
import { TodoListState } from './todoList';

export type RootState = {
    router: RouterState;
    auth: AuthState;
    todoList: TodoListState
};
