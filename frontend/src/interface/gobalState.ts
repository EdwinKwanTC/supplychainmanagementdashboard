export type JWTPayload = {
    user_id: number;
    username: string;
};
