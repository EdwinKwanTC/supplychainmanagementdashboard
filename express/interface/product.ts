export interface IProductInput {
    productName: string;
    quantity: number;
    supplierId: number;
}

export interface IReadProduct {
    id: number;
    productName: string;
    warehouse: number;
    ordered: number;
    totalOrder: number;
    exitedFactory: number;
    arrivedOutboundAirport: number;
    arrivedInboundAirport: number;
    arrivedLocalWarehouse: number;
    calledCustomerPick: number;
    arrivedCustomerAddress: number;
    customerPickedUp: number;
}
