import express from 'express';
import cors from 'cors';
import { knex } from './lib/db';
import routes from './routes';

// redis
import { client } from './lib/redis';

const app = express();

app.use(express.json({ limit: '1mb' }));
app.use(express.urlencoded({ extended: true }));

app.use(cors());
app.use(express.json());

app.use((req, res, next) => {
    console.log(req.method, req.url);
    next();
});

import { UsersService } from './services/UsersService';
import { UsersController } from './controllers/UsersController';

let usersService = new UsersService(knex);
let usersController = new UsersController(usersService);

import { ShippmentService } from './services/ShippmentService';
import { ShippmentController } from './controllers/ShippmentController';

let shippmentService = new ShippmentService(knex);
let shippmentController = new ShippmentController(shippmentService);

import { SupplierService } from './services/SupplierService';
import { SupplierController } from './controllers/SupplierController';

let supplierService = new SupplierService(knex);
let supplierController = new SupplierController(supplierService);

import { ProductService } from './services/ProductService';
import { ProductController } from './controllers/ProductController';

let productService = new ProductService(knex);
let productController = new ProductController(productService);

import { OrderService } from './services/OrderService';
import { OrderController } from './controllers/OrderController';

let orderService = new OrderService(knex);
let orderController = new OrderController(orderService);

let router = routes({
    usersController,
    shippmentController,
    supplierController,
    productController,
    orderController,
});

app.use(router);

app.get('/', (req, res) => {
    res.send({ messaeg: 'server connnected successfully' });
});

app.use((req, res) => {
    res.status(404).json({ error: 'Invalid request, typo on url or method?' });
});

const PORT = 8080;

client.on('error', (err) => console.log('Redis Client Error', err));

app.listen(PORT, () => {
    console.log(`listening on http://localhost:${PORT}`);
});
