import dotenv from 'dotenv';
dotenv.config();

// Update with your config settings.

module.exports = {
    development_sqlite: {
        client: 'sqlite3',
        connection: {
            filename: './dev.sqlite3',
        },
    },

    development: {
        client: 'mysql',
        connection: {
            database: process.env.DB_NAME,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
        },
    },

    // test: {
    //     client: 'postgresql',
    //     connection: {
    //         database: process.env.TEST_DB_NAME,
    //         user: process.env.TEST_DB_USER,
    //         password: process.env.TEST_DB_PASS,
    //     },
    //     pool: {
    //         min: 2,
    //         max: 10,
    //     },
    //     migrations: {
    //         tableName: 'knex_migrations',
    //     },
    // },

    // production: {
    //     client: 'postgresql',
    //     connection: {
    //         database: 'my_db',
    //         user: 'username',
    //         password: 'password',
    //     },
    //     pool: {
    //         min: 2,
    //         max: 10,
    //     },
    //     migrations: {
    //         tableName: 'knex_migrations',
    //     },
    // },
};
