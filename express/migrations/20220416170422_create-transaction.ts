import { Knex } from 'knex';
import tables from '../interface/tables';

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tables.SHIPPMENT);

    if (!hasTable) {
        await knex.schema.createTable(tables.SHIPPMENT, (table) => {
            table.increments();

            table.dateTime('arrived_warehouse');
            table.dateTime('received_order');
            table.dateTime('exited_factory');
            table.dateTime('arrived_outbound_airport');
            table.dateTime('arrived_inbound_airport');
            table.dateTime('arrived_local_warehouse');
            table.dateTime('called_customer_pick');
            table.dateTime('arrived_customer_address');
            table.dateTime('customer_picked_up');

            // link to product id
            table.integer('product_id').unsigned();
            table.foreign('product_id').references('product.id');

            // invoice id
            table.integer('invoice_id').unsigned().nullable();
            table.foreign('invoice_id').references('invoice.id');

            // supplier id
            table.integer('supplier_id').unsigned();
            table.foreign('supplier_id').references('supplier.id');

            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tables.SHIPPMENT);
}
