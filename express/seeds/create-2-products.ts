import { Knex } from 'knex';
import tables from '../interface/tables';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(tables.SHIPPMENT).del();
    await knex(tables.SUPPLIER).del();
    await knex(tables.PRODUCT).del();

    // Inserts seed entries
    await knex(tables.PRODUCT).insert([
        { product_name: '玩具車', status: 'launched' },
        { product_name: '飛機仔', status: 'launched' },
        { product_name: '恐龍組合', status: 'launched' },
        { product_name: '單輪車', status: 'launched' },
        { product_name: '滑板車', status: 'launched' },
        { product_name: '長板', status: 'launched' },
        { product_name: '滑板', status: 'launched' },
        { product_name: '小露寶', status: 'launched' },
        { product_name: '痴線低低B', status: 'launched' },
        { product_name: 'On9先生', status: 'launched' },
    ]);
}
