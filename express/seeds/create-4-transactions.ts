import { Knex } from 'knex';
import tables from '../interface/tables';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(tables.SHIPPMENT).del();

    const product = await knex
        .select('id')
        .from(tables.PRODUCT)
        .where('product_name', '玩具車')
        .first();
    const supplier = await knex.select('id').from(tables.SUPPLIER).first();
    const today = new Date();

    // Inserts seed entries
    for (let i = 0; i < 30; i++) {
        await knex(tables.SHIPPMENT).insert([
            { product_id: product['id'], supplier_id: supplier['id'], arrived_warehouse: today },
        ]);
    }
}
